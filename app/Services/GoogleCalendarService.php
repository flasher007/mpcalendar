<?php

namespace App\Services;

class GoogleCalendarService
{
    protected $calendarIcal;
    protected $events = [];

    public function __construct()
    {
        $this->calendarIcal = config("google.ical_url");
        $this->events = $this->getEvents();
    }

    public function getEvents()
    {
        $ical      = file_get_contents($this->calendarIcal);

        preg_match_all('/(BEGIN:VEVENT.*?END:VEVENT)/si', $ical, $result, PREG_PATTERN_ORDER);

        for ($i = 0; $i < count($result[0]); $i++) {
            $majorarray = $this->format($result[0][$i]);

            if (preg_match('/DESCRIPTION:(.*)END:VEVENT/si', $result[0][$i], $regs)) {
                preg_match('/SUMMARY:(.*)TRANSP:/si', $regs[1], $match);
                $majorarray['DESCRIPTION'] = str_replace(["\r\n", "\\"], "", $match[1]);
            }

            $this->events[] = $majorarray;
            unset($majorarray);
        }

        return $this->events;
    }

    public function render() {
        $events = [];
        foreach ($this->events as $event) {
            $date      = $event['DTSTART;VALUE=DATE'];
            $date      = str_replace('T', '', $date);
            $date      = str_replace('Z', '', $date);
            $d         = date('d', strtotime($date));
            $m         = date('m', strtotime($date));
            $y         = date('Y', strtotime($date));
            $now       = date('Y-m-d');
            $eventdate = date('d.m.Y', strtotime($date));

            if ($eventdate > $now) {
                $events[strtotime($date)][] = $event['DESCRIPTION'];
            }
        }
        ksort($events);
        return $events;
    }

    protected function format($str)
    {
        $majorarray = [];
        $tmpbyline  = explode("\r\n", $str);

        foreach ($tmpbyline as $item) {

            $tmpholderarray = explode(":", $item);
            if (count($tmpholderarray) > 1) {
                $majorarray[$tmpholderarray[0]] = $tmpholderarray[1];
            }
        }

        return $majorarray;
    }

}