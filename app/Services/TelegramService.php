<?php

namespace App\Services;


use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramService {

    public function __construct(){

    }

    public function send($text) {
        $response = Telegram::getUpdates();

        $response = Telegram::sendMessage([
            'chat_id' => '@vm_partners',
            'parse_mode' => 'Markdown',
            'text' => $text
        ]);

        return $response->getMessageId();
    }

}