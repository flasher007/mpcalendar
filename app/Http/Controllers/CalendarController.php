<?php

namespace App\Http\Controllers;

use App\Services\GoogleCalendarService;
use App\Services\TelegramService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index(TelegramService $telegramService)
    {
        $events = (new GoogleCalendarService(config('google.calendar_id')))->render();
        $res = null;
        $dt = Carbon::create(date('Y'),date('m'), date('d'),0);
        $today = $dt->timestamp;
        $addMonth = $dt->copy()->addMonth()->timestamp;
        $add2Week = $dt->copy()->addWeek(2)->timestamp;

        $text = '';
        if (isset($events[$today])) {
            $text = "\n*Успей создать рекламные кампании по инфоповодам: *\n" . implode("\n", $events[$today]);
            $text .= "\n\nХорошего заработка";
            $res = $telegramService->send($text);
        }
        if (isset($events[$add2Week])) {
            $text = "*Напоминаем инфоповоды для рекламных кампаний и идеи для макетов:*\n\n" . implode("\n", $events[$add2Week]);
            $text .= "\n\nХорошего заработка";
            $res = $telegramService->send($text);
        }
        if (isset($events[$addMonth])) {
            $text = "*Инфоповоды для рекламных кампаний и идеи для макетов:*\n\n" . implode("\n", $events[$addMonth]);
            $text .= "\n\nХорошего заработка";
            $res = $telegramService->send($text);
        }
//        if (strlen($text)) {
//            $start = "*Инфоповоды для рекламных кампаний и идеи для макетов:*\n";
//            $res = $telegramService->send($text);
//        }
        dd($res);
        return view('calendar.index', [
            'calendar' => $events
        ]);
    }
}
