<?php

Route::group(['prefix' => '/'], function () {

    Route::get('/', function () {
        return view('home');
    })->name('home');

    Route::group(['prefix' => 'tg'], function () {
        Route::get('/', 'TelegramController@index')->name('telegram.index');
    });

    Route::group(['prefix' => 'vk'], function () {
        Route::get('/', 'VkController@index')->name('vk.index');
    });

    Route::group(['prefix' => 'calendar'], function () {
        Route::get('/', 'CalendarController@index')->name('calendar.index');
    });
});

Auth::routes();